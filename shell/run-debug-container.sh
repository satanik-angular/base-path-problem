#!/usr/bin/env bash
docker run \
  --rm \
  -it \
  --pid container:base-path-app \
  --net container:base-path-app \
  --cap-add sys_admin \
  alpine \
  sh -c "cd /proc/1/root && /bin/sh"
