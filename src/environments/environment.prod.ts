export const environment = {
  production: true,
  endpoint: "${ENDPOINT}",
  debug: "${DEBUG}",
  trace: "${TRACE}"
};
