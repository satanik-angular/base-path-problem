FROM nginx:1.19.3-alpine

ARG DUMMY_URL=http://dummy-url.tld

RUN apk add --no-cache openssl=1.1.1g-r0

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
COPY conf/entrypoint.sh /
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/

ENV DUMMY_URL=${DUMMY_URL}
ENV CONTEXT_URL=/
ENV ENDPOINT http://localhost/endpoint
ENV DEBUG false
ENV TRACE false

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
