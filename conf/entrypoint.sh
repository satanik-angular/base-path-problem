#!/usr/bin/env sh

# find files with variables
variableFiles=$(grep -lrE "$(env | sed -E "s/(\w+)=.*/\\\\$\\\\{\1\\\\}/g" | tr "\n" "|" | sed 's/|$//')" /usr/share/nginx/html)

# replace variables in files
for f in ${variableFiles}; do
  envsubst "$(env | sed -E "s/(\w+)=.*/\${\1}/g" | tr "\n" "," | sed 's/,$//')" < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"
done

# normalize context url and delimit without a trailing slash
contextURL=$(echo "${CONTEXT_URL}" | sed -E 's@(://)|/$|((/)/+)@\1\3@g')

# find files that contain dummy url
pageFiles=$(grep -lr "${DUMMY_URL}" /usr/share/nginx/html)

# loop all files with the context url and patch them with the real context url
for f in ${pageFiles}; do
  sed -i -E "s@${DUMMY_URL}/?@${contextURL}/@g" "$f"
done

# find files that contain relative URLs
relFiles=$(grep -rlE ' (src|href)="?/' /usr/share/nginx/html/)

# loop all files with only relative URLs and patch them with the real context url
for f in ${relFiles}; do
  sed -i -E 's@((src|href)="?)/?@\1'${contextURL}'/@g' "$f"
done

# find all files that contain links or scripts
integrityFiles=$(grep -lrE '<(link|script)[^>]*(src|href)="?'${contextURL}'([^"> ]+)"?[^>]*>' /usr/share/nginx/html)

# then loop them
for f in ${integrityFiles}; do
  # find all the asset URLs that need a new integrity hash
  # how it works:
  #   usually sed does not allow matching multiple patterns on the same line
  #   that is because the asterisk/star-operator is greedy (and no non-greedy alternative exists)
  #   therefore it can be utilised that tags start and end with angle-brackets
  #
  #  sed-parameters and flags used:
  #
  #   -E - extended regex
  #   -n - do not pring
  #   p  - print
  #   g  - global
  #
  # steps:
  # 1.) match beginning of tag followed by any characters - not being the end
  # 2.) find src or href attributes
  # 3.) match potential quotes (not always present)
  # 4.) match the page url (is not needed and will be discarded)
  # 5.) match the relative url of the asset
  # 6.) match everything until an integrity attribute is found, that also contains a hash
  # 7.) match the end of a tag
  # 8.) replace everything match (whole tag) with only the relative url prefexed by a matchable string
  #     and pre- and suffixed by a line break, to put it on its own line
  # 9.) use awk to match matchable string and return second columen - containing the relative url
  urls=$(sed -nE 's@<(link|script)[^>]*(src|href)="?'${contextURL}'([^"> ]+)"?[^>]*>@\nURL: \3\n@gp' "$f" | awk '/URL:/ {print $2}' | uniq)

  # choose bits for integrity hash
  bits="256"

  # loop all the relative URLs found in the current file
  for url in ${urls}; do
    if ! test -f "/usr/share/nginx/html${url}"; then
      break
    fi
    # use the relative URL to get a sha256 hash of the newly patched file
    integrity=$(cat "/usr/share/nginx/html${url}" | openssl dgst -sha${bits} -binary | openssl base64 -A)
    # replace the old integrity hash value with the new one
    sed -i -E 's@('${url}'"?)([^i>]*)(integrity="?[^"> ]*"?)?@\1 integrity="sha'${bits}'-'${integrity}'"\2@g' "$f"
  done
done

# wrap original entrypoint-script and pass CMD
/docker-entrypoint.sh "$@"
