# Angular 2+ - Base Path Problem

> ### **It is important to note that all the requirements and ideas proposed are centered around a deployment on a Kubernetes cluster**

## Introduction

Angular uses two components to specify the URLs used for the `<base href>` tag, where to load the assets from and for the routing. These are named `BASE-HREF` and `DEPLOY-URL` respectively.

These can be supplied for `ng serve` and `ng build` via the command line arguments `--base-href=<url>` and `--deploy-url=<url>`. Running this app locally or on a dedicated domain on the root path, poses no problem for the default value of `/` for both the `BASE-HREF` and the `DEPLOY-URL`. When the app, however, is deployed on a host and served from a subpath, the initial loading of the `index.html` may still work, but the assets first and foremost won't load anymore, as it tries to find them from root where it cannot find them.

Henceforth, it is important to set those two URLs when deploying on an environment where the app is served on such a subpath.

## Motivation

It seems to happen seldom enough, that this problem is only tackled sparsely on the web. However in a setup such as ours, where we want to deploy the same app in multiple environments for different purposes and never knowing if a dedicated domain is guaranteed or not, we wanted to solve this problem. To make this more clear we will give some examples of such use-cases:

| Environment       | Path                                                    |
| ----------------- | ------------------------------------------------------- |
| local development | http://localhost:4200                                   |
| local integration | https://system.local/angular-app                        |
| development       | https://ourcompany.com/proxy-dev/system/angular-app     |
| staging           | https://ourcompany.com/proxy-staging/system/angular-app |
| Customer 1        | https://cluster.customer.com/angular-app                |
| Customer 2        | https://angular-app.customer2.com                       |

## Approaches

There are several solutions to this problem we have found but most of them do not yield satisfactory results, for completeness sake, however, we will still describe them, as they may proof to solve the problem for different requirements.

### "The Classic"

A classical way of deploying on Kubernetes is to build the app locally with `ng build` and then build the container with a custom Dockerfile based on an nginx image and adding a configuration that utilizes the `try_files` directive to, when the path is not found the `index.html` is served regardless to allow Angular routing.

First the app is built:

```bash
 $ ng build --prod --base-href=<target url> --deploy-url=<cdn url>`
```

The Dockerfile classically looks as follows:

```dockerfile
FROM nginx:1.19.3-alpine

EXPOSE 80

COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/
```

Here the first `COPY` directive adds the custom `nginx.conf` file. Then the target directory is cleaned and then populated by the application files with the second `COPY`. The custom nginx configuration may look as follows:

```nginx
server {
  listen 80;
  location / {
    root /usr/share/nginx/html;
    index index.html index.htm;
    try_files $uri $uri/ /index.html =404;
  }
}
```

As mentioned before, it simply sets the root to the target directory `/usr/share/nginx/html` and then uses the `try_files` directive to find the concrete asset first and if not available returns the `index.html` file.

Using this method one would end up with a different container image for every deployment environment, as the `BASE-HREF` and the `DEPLOY-URL` are hard-coded inside the image files. This results in 6 different images, exponentially increasing when different version tags are used as well.

### Init-Containers

In Kubernetes there is the concept of init-containers. These are containers started inside a pod (shared kernel namespaces etc.), before the actual containers are started. Therefore it is possible to move the build process from the local machine to the init-container by using these steps.

#### 1. Build a container with a different Dockerfile

```dockerfile
FROM node:1.19.3-alpine

# set working directory
WORKDIR /app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH

# install and cache app dependencies
COPY package*.json ./

RUN npm install

COPY / /app/

ENV PROTOCOL http
ENV HOST localhost
ENV CONTEXT_PATH /
ENV PORT 8080
VOLUME /app/dist

CMD \
  ng build \
  --prod \
  --base-href=${PROTOCOL}://${HOST}:${PORT}${CONTEXT_PATH} \
  --deploy-url=${PROTOCOL}://${HOST}:${PORT}${CONTEXT_PATH} \
  --output-path=build \
  && \
  rm -rf /app/dist/* && \
```

Here the image builds the application and moves the transpiled files to `/app/dist` inside the container which is a mounted volume.

#### 2. Add to deployment as an init-container and add a shared volume

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: deployment
  name: deployment
spec:
  replicas: 1
  selector:
    matchLabels:
      app: deployment
  template:
    metadata:
      labels:
        app: deployment
    spec:
      containers:
      - name: deployment
        image: nginx:alpine
        ports:
        - containerPort: 80
        volumeMounts:
        - name: appdir
          mountPath: /usr/share/nginx/html
        - name: confdir
          mountPath: /etc/nginx/conf.d/default.conf
          readOnly: true
          subPath: default.conf
      initContainers:
      - name: deployment-init
        image: {{ $repository }}:{{ $tag }}
        imagePullPolicy: IfNotPresent
        envFrom:
        - configMapRef:
            name: configmap
            optional: false
        volumeMounts:
        - name: appdir
          mountPath: /app/dist
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      volumes:
      - name: appdir
        emptyDir: {}
      - configMap:
          defaultMode: 420
          name: configmap
          optional: false
        name: confdir
```

The init-container uses the image built in step 1. The environment variables used in the image for the `CMD` directive are supplied by a config-map. The finally used nginx container for the deployment is specified by using the standard `nginx:alpine` image and mounting the same volume, previously mounted in the init-container at `/app/dist`, at `/usr/share/nginx/html`. Furthermore, the nginx configuration with the `try_files` directive is supplied by a mounted volume generated from the same configmap that contains the configuration as a key-value pair.

#### 3. Supply configmap

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: configmap
data:
  PROTOCOL: https
  HOST: ourcompany.com
  CONTEXT_PATH: /proxy-dev/system/angular-app
  PORT: "443"
  default.conf: |-
    server {
      listen 80;
      location / {
        root /usr/share/nginx/html;
        index index.html index.htm;
        try_files $uri $uri/ /index.html =404;
      }
    }
```

For clarity's sake the configmap is populated with the values of the 3rd proposed environment URL being https://ourcompany.com/proxy-dev/system/angular-app.

#### Evaluation

This definitely solves the *base path problem*. It is possible to build a single container image that can be deployed in different environments, regardless of the fact, whether it is proxied, and served from a root path or on a subpath.

There are, some downsides to this approach however.

1. The original Angular code is shared. Everyone having access to the init-container image or the container, can access the original code. This may or may not be a problem for different parties. But if your policy is to obfuscate your code and make reverse-engineering difficult (even though it is not entirely possible, due to the code being sent to the client on request), then this may be a problematic approach.
2. Deployment becomes infinitely slower. First of all, some `node_modules` directories have hundreds of MB. Therefore, the resulting images are in return hundreds of MB big, resulting in slow upload and download speed of the image. Furthermore, the `ng build` command is run during the start-up phase of the container. Every time. Hence, deployment increases by the long time it takes to build the Angular app (i.e. several minutes), and this for every deployment. Thinking of a Kubernetes cluster, it may be possible to introduce autoscaling to deploy the app multiple times on demand. Yet, at the time the new container is deployed the demand may have already subsided.

#### Addendum - Increase Deployment Speed

It is possible to speed up the deployment process specific in the cases of autoscaling. When `ng build` is run first the node modules are built. These can be cached by running `ng build --prod` in a `RUN` directive right before the `CMD`. This in return slows down the image build process.

#### Addendum 2 - Add Context-Dependent Configurations

By supplying extra information inside the configmap and replacing that information inside the `environment.prod.ts` file with e.g. a `sed` or `envsubst` inside the `CMD` directive of the Dockerfile it is possible to inject further context-dependent configurations to the application.

```dockerfile
ENV ENDPOINT http://endpoint
ENV DEBUG true
ENV TRACE true
...
CMD \
  sed -i -E "s@(endpoint: ['|\"])[^'\"]+@\1${ENDPOINT}@g" src/environments/environment.prod.ts \
  && \
  envsubst '${DEBUG},${TRACE}' < src/envirionments/environment.prod.ts > env.tmp && mv env.tmp src/envirionments/environment.prod.ts \
...
```

### Angular-only Solution

It is possible to pin the responsibility of finding and changing the `BASE-HREF` and `DEPLOY-URL` solely to the Angular application itself. The idea is to introduce some JavaScript code, that on initial load probes the server to find a specific file, and upon finding it, due to the relative path being known, can determine the *base-path* this way. From that Angular has to be modified to use this found *base-path* everywhere it would have been injected through `ng build` originally.

The following steps describe how we would solve this problem.

#### 1. File to probe

Add a file named `config.json` to `src/assets/config.json`. This file can contain any arbitrary data, e.g. data that is also context-dependent like other endpoints, secrets, settings for debugging etc.

```json
{
   "debug": "true",
   "endpoint": "https://kubernetes.local/endpoint"
}
```

#### 2. Code to probe

As the assets cannot be found initially, due to the fact, that the assets are not prefixed correctly with the *base-path*, the code to probe for the file is added directly to the `index.html` file, that can be retrieved.

```html
<body>
  <app-root></app-root>
  <script>
    function addScriptTag(d, src) {
      const script = d.createElement('script');
      script.type = 'text/javascript';
      script.onload = function(){
        // remote script has loaded
      };
      script.src = src;
      d.getElementsByTagName('body')[0].appendChild(script);
    }

    const pathName = window.location.pathname.split('/');

    const promises = [];

    for (const index of pathName.slice().reverse().keys()) {

      const path = pathName.slice(0, pathName.length - index).join('/');
      const url = `${window.location.origin}/${path}/assets/config.json`;
      const stripped = url.replace(/([^:]\/)\/+/gi, '$1')
      promises.push(fetch(stripped));
    }

    Promise.all(promises).then(result => {
      const response = result.find(response => response.ok && response.headers.get('content-type').includes('application/json'));
      if (response) {
        response.json().then(json => {
          const basePath = response.url.replace('assets/config.json','');
          document.querySelector('base').setAttribute('href', basePath);
          for (const node of document.querySelectorAll('script[src]')) {
            addScriptTag(document, `${basePath}/${node.getAttribute('src')}`);
            node.remove();
            window['app-config'] = Object.assign(json, {'basePath': basePath});
          }
        });
      }
    });
  </script>
</body>
```

This code retrieves the `window.location.pathname`, and traverses it upwards. The paths are then appended by `/assets/config.json` where we placed the file to probe. At some parent path it should find the `config.json` and then replace all the URLs that can be found on the `index.html` with the determined URL by the probing. Furthermore, it is possible to set a global variable that can then be accessed by Angular after reloading all the `<script>` tags (e.g. `window['app-config']`).

#### 3. Use the global variable

Replacing the `BASE-HREF` is very easy in Angular. There is a dedicated `InjectionToken` for it that can be set inside the `AppModule`s providers.

```typescript
@NgModule({
  providers: [
  {
    provide: APP_BASE_HREF,
    useFactory: () => {
      const config: AppConfig = (window as {[key: string]: any})['app-config'];
      return config.basePath;
    }
  }]
})
export class AppModule { }
```

#### Evaluation

Unfortunately it is an incomplete solution. First of all it is complicated and hard to maintain. The code is spread pretty wide and is not a one-shot solution, as different HTML tags require more logic in the code directly inside the `index.html` file. Moreover, we haven't found a way to replace the `DEPLOY-URL` as easily as we have the `BASE-HREF`.

It is elegant though, in the way, that it does not require any input during the deployment process itself.

### Re-Replace Base Path

This is a combination of the classical way and the init-container solution we proposed before. Thereby, the application will be built locally on the host or development machine and any arbitrary values are supplied for `BASE-HREF` and `DEPLOY-URL`. Hereafter, the nginx container is created as usual, but the `CMD` directive of the original base image is replaced by a more complex one, replacing the previously set arbitrary URL-values.

The following steps provide our solution to that problem.

#### 1. Build with arbitrary URL-values

```bash
$ ng build --prod --base-href=arbitrary-base-href-string --deploy-url=arbitrary-deploy-url-string
```

It is apparent that those strings should not appear anywhere else in the code or the later performed substitution would damage the code. Hence, the URL-values should be at least uncommon enough, that there is a statistically low probability of them occurring somewhere in the code, if it is maintained by another person. This may be possible by performing a check beforehand if this value is already present in the code, before performing the build.

On the other hand, it is not necessary that those values are secret or non-guessable, on the contrary, if the values are known, hopefully nobody will choose these values for variables or some other text available in the code.

#### 2. Re-Rewrite the arbitrary URL-values

In the Dockerfile the `CMD` directive is enhanced, by performing the substitutions before the nginx service is started. This may look as follows.

```dockerfile
FROM nginx:1.19.3-alpine

EXPOSE 80

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/

ENV BASE_HREF /
ENV DEPLOY_URL /

CMD \
  baseHrefFiles=$(grep -lr "arbitrary-base-href-string" /usr/share/nginx/html) \
  && \
  URL=$(echo "${BASE_HREF}/" | sed -E 's@(://)|(/)+@\1\2@g') \
  && \
  for f in ${baseHrefFiles}; do sed -i -E "s@arbitrary-base-href-string@${URL}@g" "$f"; done \
  && \
  deployUrlFiles=$(grep -lr "arbitrary-deploy-url-string" /usr/share/nginx/html) \
  && \
  URL=$(echo "${DEPLOY_URL}/" | sed -E 's@(://)|(/)+@\1\2@g') \
  && \
  for f in ${deployUrlFiles}; do sed -i -E "s@arbitrary-deploy-url-string@${URL}@g" "$f"; done \
  && \
  nginx -g 'daemon off;'
```

Of course if the `BASE-HREF` and the `DEPLOY-URL` are the same (e.g. `base-path-string`) this can be shortened to

```dock
FROM nginx:1.19.3-alpine

EXPOSE 80

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/

ENV BASE_PATH /

CMD \
  files=$(grep -lr "base-path-string" /usr/share/nginx/html) \
  && \
  URL=$(echo "${BASE_PATH}/" | sed -E 's@(://)|(/)+@\1\2@g') \
  && \
  for f in ${files}; do sed -i -E "s@base-path-string@${URL}@g" "$f"; done \
  && \
  nginx -g 'daemon off;'
```

The `grep` finds all the files where this `base-path-string` was injected. The `sed` normalizes the URL, as it is not known if the `BASE_PATH` supplied is ending in a `/` or not. The loop replaces the `base-path-string` in all the found files with the normalized URL. Then the webserver is started.

#### Evaluation

For us this was the perfect solution for several reasons.

1. The base path can be supplied easily.
2. The changes to the original code are very small.
3. The image is small. The images we used hat approximately 750MB before, while using the init-container approach. Now the image size is about 25MB, which allows fast pushing and fast downloads of these images, as well as fast export and import of image tar-files.
4. The image building is faster, as the ES-modules are cached locally during development anyways, unlike building in containers, where one always starts with an empty state when there is a small change to the package.json or package-lock.json, due to the layer system of containers, which are discarded completely on file changes.
5. The image only contains transpiled and obfuscated code, which makes it harder to reverse engineer our applications functionalities.
6. Starting up the container is blazing fast. The container starts in seconds, opposed to the init-container solution that needed minutes to be deployed.

It may lack compared to the Angular-only solution in that, it is necessary at the deployment to supply the base path explicitly. But this is neglectable in the face of all the benefits it provides.

#### Addendum

It was possible with the other approaches to add context-dependent configurations. This also provides the possibility to easily add that. The solution to that is simply supplying the container with the respective environment variables and performing more replacement in files.

An example could look like this.

```typescript
// src/app/environment.prod.ts
export const environment = {
    debug: '${DEBUG}',
    trace: '${TRACE}',
    endpoint: '${ENDPOINT}'
}
```

And then in the Dockerfile add more substitutions. The final Dockerfile would look like this.

```dockerfile
FROM nginx:1.19.3-alpine

EXPOSE 80

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/

ENV BASE_PATH /
ENV ENDPOINT http://localhost/endpoint
ENV DEBUG true
ENV TRACE true

CMD \
  mainFiles=$(ls /usr/share/nginx/html/main*.js) \
  && \
  for f in ${mainFiles}; do \
    envsubst '${ENDPOINT},${DEBUG},${TRACE}' < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"; \
  done \
  && \
  runtimeFiles=$(grep -lr "base-path-string" /usr/share/nginx/html) \
  && \
  URL=$(echo "${BASE_PATH}/" | sed -E 's@(://)|(/)+@\1\2@g') \
  && \
  for f in ${runtimeFiles}; do sed -i -E "s@base-path-string@${URL}@g" "$f"; done \
  && \
  nginx -g 'daemon off;'
```

It is known that the `environment.prod.ts` content is inside the `main*.js` files, thus it is not necessary to `grep` in all files. In the loop the `envsubst` is then used to replace the environment variables. It is necessary to save this to a temporary file and afterwards move that back to its original location.

#### Addendum 2

To make it more readable and utilize the original docker-entrypoint.sh of the nginx container and to allow patching the files with integrity-hashes one can improve by writing the Dockerfile like this.

```dockerfile
FROM nginx:1.19.3-alpine

ARG DUMMY_URL=http://dummy-url.tld

RUN apk add --no-cache openssl=1.1.1g-r0

COPY conf/nginx.conf /etc/nginx/conf.d/default.conf
COPY conf/entrypoint.sh /
RUN rm -rf /usr/share/nginx/html/*
COPY dist/ /usr/share/nginx/html/

ENV DUMMY_URL=${DUMMY_URL}
ENV CONTEXT_URL=/
ENV ENDPOINT http://localhost/endpoint
ENV DEBUG false
ENV TRACE false

EXPOSE 80

ENTRYPOINT ["/entrypoint.sh"]
CMD ["nginx", "-g", "daemon off;"]
```

Here the `openssl` binaries are installed for creating of the hashes. Another file `conf/entrypoint.sh` is added to the root of the container. A build argument named `DUMMY_URL` was added that is also reflected in the environment variables. Furthermore the `ENTRYPOINT` and `CMD` directives are overridden to use the copied `entrypoint.sh` and pass on the standard nginx-command.

The `entrypoint.sh` then looks as follows.

```bash
#!/usr/bin/env sh

# find files with variables
variableFiles=$(grep -lrE '\$\{ENDPOINT\}|\$\{DEBUG\}|\$\{TRACE\}' /usr/share/nginx/html)

# replace variables in files
for f in ${variableFiles}; do
  envsubst '${ENDPOINT},${DEBUG},${TRACE}' < "$f" > "${f}.tmp" && mv "${f}.tmp" "$f"
done

# normalize context url and delimit without a trailing slash
contextURL=$(echo "${CONTEXT_URL}" | sed -E 's@(://)|/$|((/)/+)@\1\3@g')

# find files that contain dummy url
pageFiles=$(grep -lr "${DUMMY_URL}" /usr/share/nginx/html)

# loop all files with the context url and patch them with the real context url
for f in ${pageFiles}; do
  sed -i -E "s@${DUMMY_URL}/?@${contextURL}/@g" "$f"
done

# find files that contain relative URLs
relFiles=$(grep -rlE '(src|href)="?/' /usr/share/nginx/html/)

# loop all files with only relative URLs and patch them with the real context url
for f in ${relFiles}; do
  sed -i -E 's@((src|href)="?)/?@\1'${contextURL}'/@g' "$f"
done

# find all files that contain links or scripts
integrityFiles=$(grep -lrE '<(link|script)[^>]*(src|href)="?'${contextURL}'([^"> ]+)"?[^>]*>' /usr/share/nginx/html)

# then loop them
for f in ${integrityFiles}; do
  # find all the asset URLs that need a new integrity hash
  # how it works:
  #   usually sed does not allow matching multiple patterns on the same line
  #   that is because the asterisk/star-operator is greedy (and no non-greedy alternative exists)
  #   therefore it can be utilised that tags start and end with angle-brackets
  #
  #  sed-parameters and flags used:
  #
  #   -E - extended regex
  #   -n - do not pring
  #   p  - print
  #   g  - global
  #
  # steps:
  # 1.) match beginning of tag followed by any characters - not being the end
  # 2.) find src or href attributes
  # 3.) match potential quotes (not always present)
  # 4.) match the page url (is not needed and will be discarded)
  # 5.) match the relative url of the asset
  # 6.) match everything until an integrity attribute is found, that also contains a hash
  # 7.) match the end of a tag
  # 8.) replace everything match (whole tag) with only the relative url prefexed by a matchable string
  #     and pre- and suffixed by a line break, to put it on its own line
  # 9.) use awk to match matchable string and return second columen - containing the relative url
  urls=$(sed -nE 's@<(link|script)[^>]*(src|href)="?'${contextURL}'([^"> ]+)"?[^>]*>@\nURL: \3\n@gp' "$f" | awk '/URL:/ {print $2}' | uniq)

  # choose bits for integrity hash
  bits="256"

  # loop all the relative URLs found in the current file
  for url in ${urls}; do
    if ! test -f "/usr/share/nginx/html${url}"; then
      break
    fi
    # use the relative URL to get a sha256 hash of the newly patched file
    integrity=$(cat "/usr/share/nginx/html${url}" | openssl dgst -sha${bits} -binary | openssl base64 -A)
    # replace the old integrity hash value with the new one
    sed -i -E 's@('${url}'"?)([^i>]*)(integrity="?[^"> ]*"?)?@\1 integrity="sha'${bits}'-'${integrity}'"\2@g' "$f"
  done
done

# wrap original entrypoint-script and pass CMD
/docker-entrypoint.sh "$@"
```

This file is first finds the environment variables files and then replaces those. The code afterwards is generalized, looking for all absolute occurrences of the `DUMMY_URL` and all relative paths and tries to replace them. Then it finds all occurrences of the `contextURL` and adds an integrity-hash for the file. Finally it runs the original `docker-entrypoint.sh` and passes on the arguments from the `CMD` directive. This bit is practical in the sense that it behaves as an unmodified nginx container.

## Hands-On

This repository contains a basic Angular application without any modifications except for those necessary to demonstrate approach 3 the **Re-Replace Base Path**.

```bash
# 1. check out the repository
$ git clone https://gitlab.com/satanik-angular/base-path-problem.git
# 2. install dependencies (requires node)
$ npm install
# 3. build the app
$ npm run container:ng
# 4. build the container
$ npm run container:build
# 5. replace values in .env to align with your needs
$ cat .env
BASE_PATH=http://localhost/deep/link
ENDPOINT=https://rest.example.com
DEBUG=true
TRACE=true
# 6. run the container
# the app runs on http://localhost:8080 and the index.html can be retrieved but assets will not load
$ npm run container:run
# 7. build the proxy container
# this is an nginx that forwards calls to http://localhost/deep/link to http://localhost:8080
$ npm run container:proxy:build
# 8. run the proxy container
$ npm run container:proxy:run
# 9. start a debug container and examine the files on the nginx container
# this starts a container in the same pid namespace and allows 
# browsing another containers filesystem and seeing its processes
$ npm run container:debug
$ cd /proc/1/root/usr/share/nginx/html
$ grep -no -E ".{0,10}localhost/deep/link.{0,10}" *
index.html:6:f="http://localhost/deep/link/">
index.html:9:f="http://localhost/deep/link/styles.3f
index.html:12:c="http://localhost/deep/link/runtime-e
index.html:12:c="http://localhost/deep/link/polyfills
index.html:12:c="http://localhost/deep/link/runtime-e
index.html:12:c="http://localhost/deep/link/polyfills
index.html:12:c="http://localhost/deep/link/main-es20
index.html:12:c="http://localhost/deep/link/main-es5.
runtime-es2015.b76dd66c324372428424.js:1:p="http://localhost/deep/link/";var a=w
runtime-es5.f7aad58c299c3ad46ff3.js:1:p="http://localhost/deep/link/";var a=w
$ grep -no -E ".{0,10}rest\.example\.com.{0,10}" *
main-es2015.efbfb7092c7281eb978b.js:1:="https://rest.example.com"}}var Si=
main-es5.6205bde9c88a1becd3f5.js:1:="https://rest.example.com"}}(),Ma=P
# 10. finally open the location http://localhost/deep/link
# the Angular assets should be loaded
```

